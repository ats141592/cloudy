if __name__=="__main__":
    parameter_dict = {"blackbody":["1.00e+05", "K"], "hden": "5", "luminosity":["total", "38"], "radius":"18"}

    with open("nebula.in", mode="w") as fout:
        for key, value in parameter_dict.items():
            if key=="blackbody":
                fout.write("\t".join([key, parameter_dict[key]]))