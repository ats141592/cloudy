import astropy.constants
import astropy.io.fits
import astropy.units
import matplotlib.pyplot
import numpy
import pandas
import seaborn


def setup_seaborn():
    seaborn.set_theme()
    seaborn.set_context("poster")
    seaborn.set_palette("hls", 4)
    seaborn.set_style("ticks")


if __name__=="__main__":
    dataframe               = pandas.read_table("quasar.con")
    dataframe.columns       = ["energy", "incident", "transmitted", "diffout", "netrans", "scattered", "total", "", "", "", "", ""]
    energy                  = numpy.zeros(len(dataframe))*astropy.units.keV
    luminosity_continuum    = numpy.zeros(len(dataframe))
    luminosity_total        = numpy.zeros(len(dataframe))
    luminosity_transmitted  = numpy.zeros(len(dataframe))
    luminosity_scattered    = numpy.zeros(len(dataframe))

    for i in range(len(dataframe)):
        energy[i]                   = dataframe["energy"][i]*astropy.constants.Ryd.to(astropy.units.keV, equivalencies=astropy.units.spectral())
        luminosity_total[i]         = dataframe["total"][i]
        luminosity_transmitted[i]   = dataframe["transmitted"][i]
        luminosity_scattered[i]     = dataframe["scattered"][i]

    setup_seaborn()
    fig = matplotlib.pyplot.figure  (dpi=200, figsize=(16,9))
    fig.subplots_adjust             (left=0.125, bottom=0.125, right=0.95, top=0.95)
    matplotlib.pyplot.axis          ([6.00e+00, 7.00e+00, 1.00e+40, 1.00e+44])
    matplotlib.pyplot.plot          (energy.value, luminosity_total, label="Total", linewidth=1, marker=".", markersize=4)
    matplotlib.pyplot.plot          (energy.value, luminosity_transmitted, label="Transmitted", linewidth=1, marker=".", markersize=4)
    matplotlib.pyplot.plot          (energy.value, luminosity_scattered, label="Scattered", linewidth=1, marker=".", markersize=4)
    matplotlib.pyplot.legend        (loc="upper left")
    matplotlib.pyplot.xlabel        ("Energy (keV)")
    matplotlib.pyplot.xscale        ("linear")
    matplotlib.pyplot.ylabel        ("Luminosity ($\\mathrm{erg} \\ \\mathrm{s}^{-1}$)")
    matplotlib.pyplot.yscale        ("log")
    matplotlib.pyplot.savefig       ("{0:04d}.png".format(1))
    matplotlib.pyplot.close         ()