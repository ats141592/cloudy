import subprocess


def set_parameter():
    parameter_dictionary                            = {}
    parameter_dictionary["element Lithium"]         = "off"
    parameter_dictionary["element Beryllium"]       = "off"
    parameter_dictionary["element Boron"]           = "off"
    parameter_dictionary["element Fluorine"]        = "off"
    parameter_dictionary["element Sodium"]          = "off"
    parameter_dictionary["element Aluminium"]       = "off"
    parameter_dictionary["element Phosphorus"]      = "off"
    parameter_dictionary["element Chroline"]        = "off"
    parameter_dictionary["hden"]                    = "10"
    parameter_dictionary["luminosity"]              = "42 range 1 to 10 keV"
    parameter_dictionary["radius"]                  = "12 14"
    parameter_dictionary["save continuum"]          = "\"quasar.con\""
    parameter_dictionary["save element Iron"]       = "\"quasar.fe\""
    parameter_dictionary["table power law slope"]   = "-1"
    return parameter_dictionary


if __name__=="__main__":
    filename                = "quasar"
    parameter_dictionary    = set_parameter()

    with open("{0:s}.in".format(filename), mode="w") as fout:
        for key, value in parameter_dictionary.items():
            fout.write(key + " ")
            fout.write(value + "\n")

    subprocess.run("../../source/cloudy.exe -r {0:s}".format(filename), shell=True)