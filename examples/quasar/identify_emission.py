import astropy.constants
import astropy.io.fits
import astropy.units
import numpy
import pandas
import scipy.signal


def detect_simple_peak(data):
    simple_peak_array = scipy.signal.find_peaks(data)[0]
    return simple_peak_array


def detect_clean_peak(data, simple_peak_array):
    continuum       = numpy.zeros(len(data))
    minimum_height  = 1.00e+40
    
    for i in range(len(data)):
        if i not in simple_peak_array:
            continuum[i] = data[i]
        else:
            continuum[i] = (data[i-1]+data[i+1])/2

    clean_peak_array = scipy.signal.find_peaks(data-continuum, height=minimum_height)[0]

    return clean_peak_array


def output_detected_peak(energy_array, clean_peak_array):
    for i in range(len(clean_peak_array)):
        if emin<=energy_array[clean_peak_array[i]]<=emax:
            print("Line id    : {0:s}".format(dataframe["identification"][clean_peak_array[i]]))
            print("Line energy: {0:.4e}".format(energy_array[clean_peak_array[i]]))


if __name__=="__main__":
    dataframe               = pandas.read_table("quasar.con")
    dataframe.columns       = ["energy", "incident", "transmitted", "diffout", "nettrans", "scattered", "total", "a", "b", "identification", "line", ""]
    emin                    = 6.00e+00*astropy.units.keV
    emax                    = 7.00e+00*astropy.units.keV
    energy_array            = numpy.zeros(len(dataframe))*astropy.units.keV
    luminosity_scattered    = numpy.zeros(len(dataframe))

    for i in range(len(dataframe)):
        energy_array[i]         = dataframe["energy"][i]*astropy.constants.Ryd.to(astropy.units.keV, equivalencies=astropy.units.spectral())
        luminosity_scattered[i] = dataframe["scattered"][i]

    simple_peak_array   = detect_simple_peak(data=luminosity_scattered)
    clean_peak_array    = detect_clean_peak(data=luminosity_scattered, simple_peak_array=simple_peak_array)
    output_detected_peak(energy_array=energy_array, clean_peak_array=clean_peak_array)